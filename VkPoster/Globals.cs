﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VkPoster
{
    static class Globals
    {
        public static string Login;
        public static string Password;
        public static int AppId;
        public static int DefaultDelay;
        public static long SenderId;
        public static long GroupId;
        public static int Link;
        public static string MailLogin;
        public static string MailPassword;
        public static string Sender;
        public static int Delay;
        public static void LoadData()
        {
            try
            {
                int counter = 0;
                string line;
                System.IO.StreamReader file =
                new System.IO.StreamReader(@"data.cfg");
                while ((line = file.ReadLine()) != null)
                {
                    if (counter == 0) MailLogin = line;
                    if (counter == 1) MailPassword = line;
                    if (counter == 2) Login = line;
                    if (counter == 3) Password = line;
                    if (counter == 4) AppId = Convert.ToInt32(line);
                    if (counter == 5) GroupId = Convert.ToInt64(line);
                    if (counter == 6) SenderId = Convert.ToInt64(line);
                    if (counter == 7) DefaultDelay = Convert.ToInt32(line);
                    if (counter == 8) Link = Convert.ToInt32(line);
                    if (counter == 9) Sender = line;
                    if (counter == 10) Delay = Convert.ToInt32(line);
                    counter++;
                }
                file.Close();
                if(counter != 11)
                    throw new Exception("Неверный формат файла");
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.WriteLine("Настройки успешно загружены");
            }
            catch(Exception e)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Ошибка чтения настроек \n{0}",e.Message);
                Console.ReadKey();
                System.Environment.Exit(2);
            }
        }
            
    }
}
