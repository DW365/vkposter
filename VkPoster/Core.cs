﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using VkNet;
using Limilabs.Mail;
using Limilabs.Client.IMAP;
using System.Text.RegularExpressions;

namespace VkPoster
{
    class Core
    {
        VkApi api;
        Imap imap;
        public Core(VkApi api)
        {
            this.api = api;
            imap = new Imap();
            try
            {
                imap.Connect("imap.mail.ru");
                imap.Login(Globals.MailLogin, Globals.MailPassword);
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.WriteLine("Авторизация почты прошла успешно");
            }
            catch(Exception e)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Ошибка подключения к почте /n{0}",e.Message);
                Console.ReadKey();
                System.Environment.Exit(2);
            }
        }
        public void StartMail()
        {
            for (;;)
            {
                string mail = ReadFromMail();
                if(mail != null)
                {
                    Post(mail);
                }
                System.Threading.Thread.Sleep(Globals.DefaultDelay);
            }
        }

        private string ReadFromMail()
        {
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("[{0:H:mm:ss}] Читаю почту", DateTime.Now);
            string r = null;
            try
            {
                imap.SelectInbox();
                List<long> uids = imap.Search(Flag.Unseen);
                if (uids.Count >= 1)
                {
                    var eml = imap.GetMessageByUID(uids[0]);
                    IMail mail = new MailBuilder().CreateFromEml(eml);
                    Console.ForegroundColor = ConsoleColor.Cyan;
                    Console.WriteLine("[SENDER] {0}", mail.From[0].Address);
                    if (mail.From[0].Address == Globals.Sender)
                    {
                        Console.WriteLine("[MESSAGE RAW] \n[TEXT] {0}\n[HTML] {1}", mail.Text, mail.HtmlData.Text);
                        if (mail.IsText)
                        {
                            r = mail.Text.Replace("*","");
                        }
                        else
                        {
                            string msg = mail.Html.Replace("<BR>", "\n");
                            msg = Regex.Replace(msg, "<br>", "\n");
                            msg = Regex.Replace(msg, "<(.|\n)*?>", "");
                            msg = Regex.Replace(msg, "&nbsp;", " ");
                            r = msg;
                        }
                    }
                    else
                    {
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.WriteLine("Письмо от неизвесного адресата");
                    }
                }
                else
                {
                    Console.ForegroundColor = ConsoleColor.DarkGreen;
                    Console.WriteLine("Новых писем нет");
                }
            }
            catch(Exception ex)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Ошибка чтения почты \n{0}", ex.Message);
                imap.Close();
                try
                {
                    imap = new Imap();
                    imap.Connect("imap.mail.ru");
                    imap.Login(Globals.MailLogin, Globals.MailPassword);
                }
                catch(Exception abc)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Не удалось подключиться \n{0}", abc.Message);
                }
            }
        if (Globals.Link == 1 && r != null)
        {
            int f1 = r.IndexOf("FROM");
            int n1 = r.IndexOf("League") - f1;
            if (f1 >= 0 && n1 >= 0)
                r = r.Remove(f1, n1);
        }
        return r;
        }

        private void Post(string msg)
        {
            send:
            try
            {
                api.Wall.Post(Globals.GroupId, false, true, msg, publishDate: DateTime.Now.AddMinutes(Globals.Delay));
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine("Успешно опубликовано");
                Console.ForegroundColor = ConsoleColor.DarkYellow;
                Console.WriteLine("[TEXT]\n{0}",msg);
            }
            catch (Exception e)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("[{0:H:mm:ss}] Ошибка: \n{1}", DateTime.Now, e.Message);
                auth:
                try
                {
                    api.Authorize(Globals.AppId, Globals.Login, Globals.Password, VkNet.Enums.Filters.Settings.All);
                    Console.ForegroundColor = ConsoleColor.Yellow;
                    Console.WriteLine("Авторизация вконтакте прошла успешно");
                }
                catch (Exception ex)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Ошибка авторизации вконтакте \n {0}", ex.Message);
                    goto auth;
                }
                goto send;
            }
        }
    }
}
