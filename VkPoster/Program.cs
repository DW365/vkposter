﻿using System;
using System.Threading.Tasks;
using VkNet;
using System.IO;


namespace VkPoster
{
    class Program
    {
        static void Main(string[] args)
        {
            Globals.LoadData();
            var vk = new VkApi();
            try
            {
                vk.Authorize(Globals.AppId, Globals.Login, Globals.Password, VkNet.Enums.Filters.Settings.All);
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.WriteLine("Авторизация вконтакте прошла успешно");
            }
            catch (Exception e)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Ошибка авторизации вконтакте \n {0}", e.Message);
                Console.ReadKey();
                System.Environment.Exit(1);
            }
            Core core = new Core(vk);
            core.StartMail();
        }
    }
}
